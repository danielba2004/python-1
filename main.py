# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import calendar
import struct

import test

HANGMAN_PHOTOS = {0: 'x-------x', 1: 'x-------x\n|\n|\n|\n|\n|',
                  2: 'x-------x\n|       |\n|       0\n|\n|\n|',
                  3: 'x-------x\n|       |\n|       0\n|       |\n|\n|',
                  4: 'x-------x\n|       |\n|       0\n|      /|\\\n|\n|',
                  5: 'x-------x\n|       |\n|       0\n|      /|\\\n|      /\n|',
                  6: 'x-------x\n|       |\n|       0\n|      /|\\\n|      / \\\n|'}

HANGMAN_ASCII_ART = "welcome to the game Hangman" + """\n  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \\ / _` | '_ ` _ \\ / _` | '_ \\ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\\__,_|_| |_|\\__, |_| |_| |_|\\__,_|_| |_|
                      __/ |                      
                     |___/"""

MAX_TRIES = 6


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


def print_hang_man(tries):
    if(tries == 0):
        print("""
    x-------x""")
    elif(tries == 1):
        print("""
    x-------x
    |
    |
    |
    |
    |""")
    elif tries == 2:
        print("""
    x-------x
    |       |
    |       0
    |
    |
    |""")
    elif tries == 3:
        print("""
    x-------x
    |       |
    |       0
    |       |
    |
    |
""")
    elif tries == 4:
        print("""
    x-------x
    |       |
    |       0
    |      /|\
    |
    |""")
    elif tries == 5:
        print("""
    x-------x
    |       |
    |       0
    |      /|\
    |      /
    |""")

    elif tries == 6:
        print("""
    x-------x
    |       |
    |       0
    |      /|\
    |      / \
    |""")



def last_early(my_str):
    tav = str.upper(my_str[-1])
    if tav in my_str[:-1]:
        return True
    else:
        return False


def distance(num1, num2, num3):
    if abs(num2 - num1) == 1 or abs(num3 - num1) == 1:
        return True
    elif (abs(num2 - num1) > 2 and abs(num2 - num3) > 2) or (abs(num3 - num1) > 2 and abs(num3 - num2) > 2):
        return True
    else:
        return False


def filter_teens(a, b, c):
    return fix_age(a) + fix_age(b) + fix_age(c)


def fix_age(age):
    if 13 <= age <= 19:
        if age != 15 and age != 16:
            return 0
    return age


def chocolate_maker(small, big, x):
    if big * 5 + small < x:
        return False
    elif x % 5 > small:
        return False
    else:
        return True


def is_valid_input(letter_guessed):
    if not letter_guessed.isalpha():
        if len(letter_guessed) > 1:
            return False
        else:
            return False
    elif len(letter_guessed) > 1:
        return False
    else:
        return True


def shift_left(my_list):
    new_list = list(my_list[1:])
    new_list.append(my_list[0])
    return new_list


def format_list(my_list):
    str = ', '.join(my_list[:-1:2])
    str = str + " and " + my_list[-1]
    return str


def extend_list_x(list_x, list_y):
    temp_list = []
    temp_list[:] = list_x
    list_x[:len(list_y)] = list_y
    list_x[len(list_y):] = temp_list
    return list_x


def are_lists_equal(list1, list2):
    if (sorted(list1) == sorted(list2)):
        return True
    else:
        return False


def longest(my_list):
    my_list.sort(reverse=True, key=len)
    return my_list[0]


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    this function return true if the string letter_guessed is made up from no more
    than 1 letter and if it is an alpha-betic letter and if it doesnt apear in the
    old_letter_guesed list
    param letter_guessed: the letter that the player guessed
    param old_letters_guessed: list of all the letter the player already guessed
    type letter_guessed: string
    type old_letters_guessed: list
    :return: true if the string letter_guessed is made up from no more
    than 1 letter and if it is an alpha-betic letter and if it doesn't apear in the
    old_letter_guesed list else return false
    """
    if is_valid_input(letter_guessed):
        if str.upper(letter_guessed) not in old_letters_guessed:
            return True
        else:
            return False


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    if check_valid_input(letter_guessed, old_letters_guessed):
        old_letters_guessed.append(str.upper(letter_guessed))
        return True
    else:
        print(str.upper(letter_guessed))
        print("->".join(old_letters_guessed))
        return False


def squared_numbers(start, stop):
    list = []
    while start <= stop:
        list += [start ** 2]
        start += 1
    return list


def is_greater(my_list, n):
    new_list = []
    for item in my_list:
        if item > n:
            new_list += [item]
    return new_list


def numbers_letters_count(my_str):
    new_list = [0, 0]
    for tav in my_str:
        if str.isdigit(tav):
            new_list[0] += 1
        else:
            new_list[1] += 1
    return new_list


def seven_boom(end_number):
    """
    this function mimiks the seven-boom game.
    :param end_number: the last number to be added to the list
    type end_number: int
    :return: a list of the nuumber between 0 and end_number but replaces the numbers
    that divided by 7 and the number that has 7 in ine of their digits with the word 'boom'
    typer: list
    """
    flag = False
    new_list = []
    for num in range(0, end_number):
        if num % 7 == 0:
            new_list += ['boom']
            continue
        new_num = num
        while new_num >= 10:
            if new_num % 10 == 7:
                new_list += ['boom']
                flag = True
                break
            else:
                new_num /= 10
        if not flag:
            new_list += [num]
        else:
            flag = False
    return new_list


def sequence_del(my_str):
    string = ''
    string += my_str[0]
    for tav in my_str[1:]:
        if (tav != string[-1]):
            string += tav
    return string


def shopping(shopping_list):
    """
    this function lets you preform 8 actions in your shopping list
    :param shopping_list: string for the items in the shopping list
    type shopping_list: string
    :return: none
    """
    num = 0
    new_list = shopping_list.split(',')
    while num != 9:
        num = int(input("enter number: "))
        if num == 1:
            print('enterd 1')
            for item in new_list:
                print(item)
        if num == 2:
            print("there are " + str(len(new_list)) + ' items in the list')
        if num == 3:
            name = input("enter item name: ")
            if name in new_list:
                print("the item " + name + " is in the list")
            else:
                print("the item " + name + " is not in the list")
        if num == 4:
            name = input("enter item name: ")
            if name in new_list:
                print("the item appears " + str(new_list.count(name)) + " times is in the list")
            else:
                print("the item " + name + " is not in the list")
        if num == 5:
            name = input("enter item to delete: ")
            if name in new_list:
                new_list.remove(name)
            else:
                print("the item " + name + " is not in the list")
        if num == 6:
            new_list.append(input("enter item name: "))
        if num == 7:
            for item in new_list:
                if len(item) < 3 or not str.isalpha(item):
                    print(item)
        if num == 8:
            list2 = []
            for item in new_list:
                if item not in list2:
                    list2 += [item]
            new_list = list2


def arrow(my_char, max_length):
    str1 = ""
    for num in range(max_length):
        str1 += (((my_char + " ") * num) + "\n")
    for num2 in range(max_length, 0, -1):
        str1 += (((my_char + " ") * num2) + "\n")
    return str1


def show_hidden_word(secret_word, old_letters_guessed):
    my_str = ''
    for letter in secret_word:
        if str.upper(letter) in old_letters_guessed:
            my_str += letter + " "
        else:
            my_str += '_ '
    return my_str


def check_win(secret_word, old_letters_guessed):
    for letter in secret_word:
        if str.upper(letter) not in old_letters_guessed:
            return False
    return True


def sort_by_price(tuple):
    return tuple[1]


def sort_prices(list_of_tuples):
    return sorted(list_of_tuples, key=sort_by_price)


def mult_tuple(tuple1, tuple2):
    new_tuple = ()
    for item in tuple1:
        for item2 in tuple2:
            new_tuple += ((item, item2),)
    for item in tuple2:
        for item2 in tuple1:
            new_tuple += ((item, item2),)
    return new_tuple


def angrema(word1, word2):
    list1 = []
    list2 = []
    for letter in word1:
        list1 += letter
    for letter in word2:
        list2 += letter
    list1.sort()
    list2.sort()
    return list2 == list1


def sort_anagrams(list_of_strings):
    new_list = []
    flag = False
    for word in list_of_strings:
        for existing_lists in new_list:
            if angrema(word, existing_lists[0]):
                print("existed " + word + " " + existing_lists[0] + " flag = " + str(flag))
                existing_lists += [word]
                flag = True
        if not flag:
            print("not existed " + word)
            new_list += [[word]]
        flag = False
    return new_list


def count_chars(my_str):
    dict = {}
    for letter in my_str:
       if letter in dict:
           dict[letter] += 1
       else:
           dict[letter] = 1
    return dict


def inverse_dict(my_dict):
    dict = {}
    for item in my_dict.items():
        if item[1] in dict:
            dict[item[1]] += [item[0]]
        else:
            dict[item[1]] = [item[0]]
    return dict


def print_hangman(num_of_tries):
    print(HANGMAN_PHOTOS[num_of_tries])


def are_files_equal(file1, file2):
    f1 = open(file1, 'r')
    f2 = open(file2, 'r')
    l1 = f1.readlines()
    l2 = f2.readlines()
    f1.close()
    f2.close()
    return l1 == l2


def files_operations(file_path, operation):
    with open(file_path, 'r') as file:
        if operation == 'sort':
            list1 = (file.read().split())
            list1.sort()
            print(list1)
        elif operation == 'rev':
            for line in file.readlines():
                print(line[::-1])
        elif operation == 'last':
            num = int(input("enter lines: "))
            text = file.readlines()
            lines = len(text)
            for line in text[lines-num:]:
                print(line)
        else:
            print('wrong operation')


def copy_file_content(source, destination):
    text = ''
    with open(source, 'r') as f1:
        text = f1.read()
    with open(destination, 'w') as f2:
        f2.write(text)


def who_is_missing(file_name):
    with open(file_name, 'r') as file:
        list1 = (file.read().split())
        list1.sort()
        for num in range(1,len(list1)):
            if list1[num] != num:
                with open(r"C:\temp text file\found.txt", 'w') as file2:
                    file2.write(str(num))
                return num


def my_mp3_playlist(file_path):
    with open(file_path, 'r') as f:
        lines_list = f.read().split(';\n')
        songs_num = 0
        max_length = 0
        artists_dir = {}
        song = ''
        for line in lines_list:
            info_list = line.split(';')
            songs_num += 1
            if info_list[1] in artists_dir:
                artists_dir[info_list[1]] += 1
            else:
                artists_dir[info_list[1]] = 1
            time_list = info_list[2].split(':')
            time = float(time_list[0])+(float(time_list[1])/100)
            if time > max_length:
                max_length = time
                song = info_list[0]
        max_times = 0
        artist_max = ''
        for artist in artists_dir.items():
            if artist[1] > max_times:
                max_times = artist[1]
                artist_max = artist[0]
        tuple1 = (song,songs_num,artist_max)
        return tuple1


def my_mp4_playlist(file_path, new_song):
    list_of_info = []
    num_of_lines = 0
    with open(file_path, 'r') as f:
        lines_list = f.read().split(';\n')
        num_of_lines = len(lines_list)
        for line in lines_list:
            info_list = line.split(';')
            list_of_info += [info_list]
    with open(file_path, 'w') as f:
        while num_of_lines < 3:
            list_of_info += [[''],[''],['']]
            num_of_lines += 1
        list_of_info[2][0] = new_song
        lines_list = []
        for list1 in list_of_info:
            lines_list += [";".join(list1)]
        for line in lines_list[:-1]:
            f.write(line + ";\n")
    with open(file_path, 'r') as f:
        print(f.read())


def choose_word(file_path, index):
    word_list_duplicates = []
    word_list = []
    with open(file_path, 'r') as f:
        word_list_duplicates = f.read().split()
        for word in word_list_duplicates:
            if word not in word_list:
                word_list += [word]
        word = word_list_duplicates[((index-1) % len(word_list_duplicates))]
        return len(word_list), word





def func(num1, num2):
    """this function returns the multiplier  of num1 and num2
     minus num1.
     param num1: first number
     param num2: second number
     type num1: int
     type num2: int
     return: the multiplier  of num1 and num2 minus num1
     rtype: int
     """
    #
    return num1 * num2 - num1


def main():
    #print("\"Shuffle, Shuffle, Shuffle\", say it together! \nChange colors and directions,\
    #       \nDon't back down and stop the player! \n\tDo you want to play Taki? \n\tPress y\\" + "n")

    # משימה מתגלגלת
    # print(HANGMAN_ASCII_ART + "\n" + str(MAX_TRIES))
    #
    # length = len(input("enter a word for underscor: "))
    # print('_ '*length)

    # char = input("Guess a letter: ").upper()
    # print(char)

    # char = str.upper(input("Guess a letter: "))
    # if(not char.isalpha()):
    #     if(len(char) > 1):
    #         print("E3")
    #     else:
    #         print("E2")
    # elif(len(char) > 1):
    #     print("E1")
    # else:
    #     print(char)

    # print(show_hidden_word("mamamia", ['a', 'i', 'c']))

    # print_hangman(3)

    # print(choose_word(r"C:\temp text file\words.txt", 15))

    print(HANGMAN_ASCII_ART + "\n" + str(MAX_TRIES))
    path = input("enter the path to the words file: ")
    index = input("enter index: ")
    print("lets start!")
    old_letters_guessed = []
    secret_word = choose_word(r"C:\temp text file\words.txt", int(index))[1]
    num_of_tries = 0
    print(HANGMAN_PHOTOS[num_of_tries])
    print(show_hidden_word(secret_word, old_letters_guessed))



    while not check_win(secret_word, old_letters_guessed):
        print(old_letters_guessed)
        letter = input("guess a letter")
        try_update_letter_guessed(letter, old_letters_guessed)
        if letter not in secret_word:
            print(":(\n")
            num_of_tries += 1
            print(HANGMAN_PHOTOS[num_of_tries] + "\n")
            if num_of_tries == MAX_TRIES:
                break
        print(show_hidden_word(secret_word, old_letters_guessed))
    if check_win(secret_word, old_letters_guessed):
        print("WON")
    else:
        print("LOST")

#end







    #my_mp4_playlist(r"C:\temp text file\songs.txt",'the best song')

    #print(my_mp3_playlist(r"C:\temp text file\songs.txt"))

    #print(who_is_missing(r"C:\temp text file\f4.txt"))

    #copy_file_content(r"C:\temp text file\copy.txt", r"C:\temp text file\paste.txt")

    #files_operetions(r"C:\temp text file\f3.txt", 'sort')
    #files_operetions(r"C:\temp text file\f3.txt", 'rev')
    #files_operetions(r"C:\temp text file\f3.txt", 'last')

    # print(are_files_equal(r"C:\temp text file\f1.txt",r"C:\temp text file\f2.txt"))

    # print(inverse_dict({'I': 3, 'love': 3, 'self.py!': 2}))

    #print(count_chars('lldkcsc'))

    # dict = {'first_name': 'maria', 'last_name': 'carey', 'birth_day': '27.03.1970',
    #         'hobbies': ['sing', 'compose', 'act']}
    # num = int(input('enter number: '))
    # if num == 1:
    #     print("hello")
    #     print(dict['last_name'])
    # elif num == 2:
    #     print(dict['birth_day'][3:5])
    # elif num == 3:
    #     print(len(dict['hobbies']))
    # elif num == 4:
    #     print(dict['hobbies'][-1])
    # elif num == 5:
    #     dict['hobbies'] += ['cooking']
    # elif num == 6:
    #     dict['birth_day'] = (27, 3, 1970)
    #     print(dict['birth_day'])
    # elif num == 7:
    #     # print(type(dict['birth_day']))
    #     if(type(dict['birth_day']) == str):
    #         dict['age'] = 2023 - int(dict['birth_day'][-4:])
    #     else:
    #         dict['age'] = 2023 - int(dict['birth_day'][2])
    #     print(dict['age'])


# print(dict['last_name'])
# print(dict['birth_day'][3:5])
# print(len(dict['hobbies']))
# print(dict['hobbies'][-1])
# dict['hobbies'] += ['cooking']
# dict['birth_day'] = (27, 3, 1970)
# print(dict['birth_day'])
# dict['age'] = 2023 - dict['birth_day'][2]
# print(dict['age'])

# print(sort_anagrams(['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating',
#                      'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening',
#                      'lasted', 'resmelts']))

# print(mult_tuple((1, 4, 5), (6, 7, 8)))

# print(sort_prices([("egg", 5.5), ("foulr", 4.9), ("cola", 9.0)]))

# data = ("self", "py", 1.543)
# format_string = "Hello %s.%s learner you have only %.1f units left before you master the course"
# print(format_string % data)

# print(arrow('#',6))

# shopping("banana,milk,butter,solt,flour,yogurts,banana,cola,cola")

# print(sequence_del("rrrrhhjjdjjjd"))

# print(seven_boom(50))

# print(squared_numbers(2,5))

# print(longest(['rrrr','ff','dddddd']))

# print(are_lists_equal([1.1, 3, 6.7], [6.7, 3, 1.1]))

# list_x = [1,2,3]
# list_y = [4, 5, 6]
# extend_list_x(list_x, list_y)
# print(list_x)

# print(format_list(['aa', 'bb', 'cc', 'dd', 'hh']))

# print(shift_left([2.01, 'gg', 66]))

# date = input("enter date: ")
# print(int(date[6:]), int(date[3:5]), int(date[0:2]))
# day = calendar.weekday(int(date[6:]), int(date[3:5]), int(date[0:2]))
# if(day == 0):
#     print("sunday")
# elif(day == 1):
#     print("monday")
# elif(day == 2):
#     print("tuesday")
# elif(day == 3):
#     print("wednesday")
# elif(day == 4):
#     print("tuesday")
# elif(day == 5):
#     print("friday")
# else:
#     print("saturday")
# print(day)

# temp = input("enter temperture: ")
# temp_num = int(temp[:-1])
# type = temp[-1]
# if(type == 'C'):
#     temp2 = str((temp_num*9 + (32*5))/5)
#     type = 'F'
# else:
#     temp2 = str((temp_num*5 - 160)/9)
#     type = 'C'
#
# print(temp2+type)

# word = input("enter: ")
# if(word[-1::-1] == word):
#     print("OK")
# else:
#     print("NOT")

# string = input("Please enter a string: ")
#     # print(string[:len(string)//2] + string[(len(string)//2):].upper())

# string = input("Please enter a string: ")
# first_char = string[0]
# print(string[0]+string[1:].replace(first_char,'e'))

# encrypted_message = "!XgXnXiXcXiXlXsX XnXoXhXtXyXpX XgXnXiXnXrXaXeXlX XmXaX XI"
# print(encrypted_message[-1::-2])

# char = input("Guess a letter: ").upper()
# print(char)

# num = input("Enter three digits (each digit for one pig): ")
# print(int(num)//100 + (int(num)%100)//10 + int(num)%10 )
# print((int(num)//100 + (int(num)%100)//10 + int(num)%10)//3 )
# print(int(num)%3 )
# print(int(num)%3  == 0)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

